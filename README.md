CRAZY PHARMACIST - FINAL PROJECT

This is a final project of Praktikum Jaringan Komputer.
The project is about a 3D online-multiplayer game in which we have to implement some conditions given by the lecturer.

Name : Crazy Pharmacist

Made by :

1. Niken Ayu Anggarini          (4210181003)
2. Ilham Sugeng Prayogi         (4210181022)
3. Frederiko Adrian Rio Bangun  (4210181024)

Gameplay : In this game, both players have to race against each other to fulfill customers' orders. 
The first player who can fulfill the order will be given a point summed up to their score.
The player with a higher score will be the winner of this game.