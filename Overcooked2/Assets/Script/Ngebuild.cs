﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ngebuild : MonoBehaviour
{
    public string url;

    public List<string> gameObjectString;
    [SerializeField] private GameObject downloadAssetText;
    [SerializeField] private GameObject LANUI;
    [SerializeField] private GameObject OnlineUI;
    void Start()
    {
        StartCoroutine(GetAssetBundle());
        Debug.Log("pushed");
    }

    IEnumerator GetAssetBundle()
    {
        WWW www = new WWW(url);
        yield return www;

        Debug.Log("entered");

        AssetBundle bundle = www.assetBundle;
        if (www.error == null)
        {
            GameObject red_1 = (GameObject)bundle.LoadAsset(gameObjectString[0]);
            Instantiate(red_1);
            GameObject red_2 = (GameObject)bundle.LoadAsset(gameObjectString[1]);
            Instantiate(red_2);
            GameObject red_3 = (GameObject)bundle.LoadAsset(gameObjectString[2]);
            Instantiate(red_3);
            GameObject red_4 = (GameObject)bundle.LoadAsset(gameObjectString[3]);
            Instantiate(red_4);
            GameObject red_5 = (GameObject)bundle.LoadAsset(gameObjectString[4]);
            Instantiate(red_5);
        }
        else
            Debug.Log(www.error);

        Debug.Log("finished");
        downloadAssetText.SetActive(false);
        LANUI.SetActive(true);
    }
}