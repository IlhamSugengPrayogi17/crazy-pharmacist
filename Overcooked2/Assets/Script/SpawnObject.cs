﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObject : MonoBehaviour {

	public GameObject cubeSmall;
	public GameObject cubeBig;
	public GameObject sphereSmall;
	public GameObject sphereBig;

	public GameObject table1;
	public Transform posTable1;
	public GameObject table2;
	public Transform posTable2;
	public GameObject table3;
	public Transform posTable3;
	public GameObject table4;
	public Transform posTable4;

	public float distanceFromPlayer = 4f;

	void Start(){
		table1 = GameObject.FindGameObjectWithTag ("meja1");
		posTable1 = table1.GetComponent<Transform> ();
		table2 = GameObject.FindGameObjectWithTag ("meja2");
		posTable2 = table2.GetComponent<Transform>();
		table3 = GameObject.FindGameObjectWithTag ("meja3");
		posTable3 = table3.GetComponent<Transform> ();
		table4 = GameObject.FindGameObjectWithTag ("meja4");
		posTable4 = table4.GetComponent<Transform> ();
	}

	void Update () {
		if(this.gameObject.transform.position.x >= posTable1.transform.position.x - 3 && (this.gameObject.transform.position.z <= posTable1.transform.position.z + 4) && (this.gameObject.transform.position.z >= posTable1.transform.position.z - 4)){
			Debug.Log ("Dekat Meja-1");
		}

		if (Input.GetKeyDown(KeyCode.Alpha1) && (this.gameObject.transform.position.x >= posTable1.transform.position.x - 3 && (this.gameObject.transform.position.z <= posTable1.transform.position.z + 4) && (this.gameObject.transform.position.z >= posTable1.transform.position.z - 4)))
		{
			Instantiate(cubeSmall, transform.position + (transform.forward * distanceFromPlayer), Quaternion.identity);
		}

		if(this.gameObject.transform.position.z <= posTable2.transform.position.z + 4 && (this.gameObject.transform.position.x <= posTable2.transform.position.x + 4) && (this.gameObject.transform.position.x >= posTable2.transform.position.x - 4)){
			Debug.Log ("Dekat Meja-2");
		}

		if(this.gameObject.transform.position.x <= posTable3.transform.position.x + 3 && (this.gameObject.transform.position.z <= posTable3.transform.position.z + 4) && (this.gameObject.transform.position.z >= posTable3.transform.position.z - 4)){
			Debug.Log ("Dekat Meja-3");
		}

		if (Input.GetKeyDown(KeyCode.Alpha3) && (this.gameObject.transform.position.x <= posTable3.transform.position.x + 3 && (this.gameObject.transform.position.z <= posTable3.transform.position.z + 4) && (this.gameObject.transform.position.z >= posTable3.transform.position.z - 4)))
		{
			Instantiate(cubeBig, transform.position + (transform.forward * distanceFromPlayer), Quaternion.identity);
		}

		if(this.gameObject.transform.position.z >= posTable4.transform.position.z - 4 && (this.gameObject.transform.position.x <= posTable4.transform.position.x + 4) && (this.gameObject.transform.position.x >= posTable4.transform.position.x - 4)){
			Debug.Log ("Dekat Meja-4");
		}

		if (Input.GetKeyDown(KeyCode.Alpha4) && (this.gameObject.transform.position.z >= posTable4.transform.position.z - 4 && (this.gameObject.transform.position.x <= posTable4.transform.position.x + 4) && (this.gameObject.transform.position.x >= posTable4.transform.position.x - 4)))
		{
			Instantiate(cubeSmall, transform.position + (transform.forward * distanceFromPlayer), Quaternion.identity);
		}
	}
}
