﻿using Project.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectorCollide : MonoBehaviour
{
    private bool isCollidedWithObj1 = false;
    private bool isCollidedWithObj2 = false;
    private bool isCollidedWithObj3 = false;
    private bool isCollidedWithObj4 = false;

    [SerializeField] private ValueManager valueManager;
    private void Update()
    {
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Bullet(Clone)")
        {
            isCollidedWithObj2 = true;
            Destroy(collision.gameObject);
        }
        else if (collision.gameObject.name == "Cube 1(Clone)")
        {
            isCollidedWithObj1 = true;
            Destroy(collision.gameObject);
        }
        else if (collision.gameObject.name == "Cube 3(Clone)")
        {
            isCollidedWithObj3 = true;
            Destroy(collision.gameObject);
        }
        else if (collision.gameObject.name == "Cube 4(Clone)")
        {
            isCollidedWithObj4 = true;
            Destroy(collision.gameObject);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.name == ("GrabPoint") && Input.GetKey(KeyCode.Return))
        {
            if (isCollidedWithObj3 && isCollidedWithObj4 && isCollidedWithObj2)
            {
                valueManager.SpawnObj5();
                isCollidedWithObj1 = false;
                isCollidedWithObj2 = false;
                isCollidedWithObj3 = false;
                isCollidedWithObj4 = false;
            }
            if (isCollidedWithObj3 && isCollidedWithObj1 && isCollidedWithObj2)
            {
                valueManager.SpawnObj6();
                isCollidedWithObj1 = false;
                isCollidedWithObj2 = false;
                isCollidedWithObj3 = false;
                isCollidedWithObj4 = false;
            }
            if (isCollidedWithObj2 && isCollidedWithObj4 && isCollidedWithObj1)
            {
                valueManager.SpawnObj7();
                isCollidedWithObj1 = false;
                isCollidedWithObj2 = false;
                isCollidedWithObj3 = false;
                isCollidedWithObj4 = false;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        

        //if (other.name == "Cube 1(Clone)")
        //{
        //    //ValueManager.value += 3;
        //    //Destroy(GameObject.FindWithTag("grabable"));
        //}

        //if (other.name == "Cube 3(Clone)")
        //{
        //    ValueManager.value += 5;
        //    Destroy(GameObject.FindWithTag("grabable"));
        //}
    }

}
