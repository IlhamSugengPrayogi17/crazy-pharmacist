﻿using Project.Utility;
using Project.Utility.Attributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Project.Networking
{
    [RequireComponent(typeof(NetworkIdentity))]
    public class NetworkTransformObj : MonoBehaviour
    {
        [SerializeField]
        [GreyOut]
        private Vector3 oldPosition;

        private NetworkIdentity networkIdentity;
        private Player obj;

        private float stillCounter = 0;

        // Start is called before the first frame update
        public void Start()
        {
            networkIdentity = GetComponent<NetworkIdentity>();
            oldPosition = transform.position;
            obj = new Player();
            obj.position = new Position();
            obj.position.x = 0;
            obj.position.y = 0;
            obj.position.z = 0;

            if (!networkIdentity.IsControlling())
            {
                enabled = true;
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (!networkIdentity.IsControlling())
            {
                if (oldPosition != transform.position)
                {
                    oldPosition = transform.position;
                    stillCounter = 0;
                    sendData();
                }
                else
                {
                    stillCounter += Time.deltaTime;
                    if (stillCounter >= 1)
                    {
                        stillCounter = 0;
                        sendData();
                    }
                }
            }
        }
        private void sendData()
        {
            //Update player information
            obj.id = gameObject.GetComponent<NetworkIdentity>().GetID();
            //Debug.Log("obj id : " + obj.id);
            obj.position.x = transform.position.x.TwoDecimals();
            obj.position.y = transform.position.y.TwoDecimals();
            obj.position.z = transform.position.z.TwoDecimals();

            networkIdentity.GetSocket().Emit("updatePositionObj", new JSONObject(JsonUtility.ToJson(obj)));
        }
    }
}
