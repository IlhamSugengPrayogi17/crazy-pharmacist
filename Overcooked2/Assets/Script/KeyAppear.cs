﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyAppear : MonoBehaviour {

	public GameObject Item;
	public Transform Spawner;
	public bool IsSpawnable = false;

	public void OnTriggerEnter(Collider other)
	{ 
		if (other.tag == "Player")
		{
			IsSpawnable = true;
		}
	}

	public void OnTriggerExit(Collider other)
	{
		if (other.tag == "Player")
		{
			IsSpawnable = false;
		}
	}

	public void Update()
	{
		if (Input.GetKeyDown(KeyCode.Alpha9))
		{
			if (IsSpawnable)
			{

				Instantiate(Item, Spawner.position, Spawner.rotation);

			}

		}
	}
}
