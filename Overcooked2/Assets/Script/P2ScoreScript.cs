﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class P2ScoreScript : MonoBehaviour
{
    [SerializeField] private Text scoreP2;
    [SerializeField] private Text reqTextP2;
    [SerializeField] private GameObject P2Win;
    private int score;
    int rand;

    public bool Obj5isNeeded;
    public bool Obj6isNeeded;
    public bool Obj7isNeeded;
    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        rand = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (rand == 1)
        {
            reqTextP2.text = "Need Cyan";
            Obj5isNeeded = true;
        }
        else if (rand == 2)
        {
            reqTextP2.text = "Need Yellow";
            Obj6isNeeded = true;
        }
        else if (rand == 3)
        {
            reqTextP2.text = "Need Magenta";
            Obj7isNeeded = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "Cyan(Clone)" && Obj5isNeeded)
        {
            score++;
            scoreP2.text = score.ToString();
            Destroy(collision.gameObject);
            Obj5isNeeded = false;
            rand = 2;
        }
        else if (collision.gameObject.name == "Yellow(Clone)" && Obj6isNeeded)
        {
            score++;
            scoreP2.text = score.ToString();
            Destroy(collision.gameObject);
            Obj6isNeeded = false;
            rand = 3;
        }
        else if (collision.gameObject.name == "Magenta(Clone)" && Obj7isNeeded)
        {
            score++;
            scoreP2.text = score.ToString();
            Destroy(collision.gameObject);
            Obj7isNeeded = false;
            rand = 1;
        }

        if (score == 3)
        {
            P2Win.SetActive(true);
        }
    }
}
