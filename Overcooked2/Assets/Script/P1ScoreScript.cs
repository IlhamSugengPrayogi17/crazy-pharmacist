﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class P1ScoreScript : MonoBehaviour
{
    [SerializeField] private Text scoreP1;
    [SerializeField] private Text reqTextP1;
    [SerializeField] private GameObject P1Win;
    private int score;
    int rand;

    public bool Obj5isNeeded;
    public bool Obj6isNeeded;
    public bool Obj7isNeeded;
    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        rand = 2;
    }

    // Update is called once per frame
    void Update()
    {
        if (rand == 1)
        {
            reqTextP1.text = "Need Cyan";
            Obj5isNeeded = true;
        }
        else if (rand == 2)
        {
            reqTextP1.text = "Need Yellow";
            Obj6isNeeded = true;
        }
        else if (rand == 3)
        {
            reqTextP1.text = "Need Magenta";
            Obj7isNeeded = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Cyan(Clone)" && Obj5isNeeded)
        {
            score++;
            scoreP1.text = score.ToString();
            Destroy(collision.gameObject);
            Obj5isNeeded = false;
            rand = 2;
        }
        else if (collision.gameObject.name == "Yellow(Clone)" && Obj6isNeeded)
        {
            score++;
            scoreP1.text = score.ToString();
            Destroy(collision.gameObject);
            Obj6isNeeded = false;
            rand = 3;
        }
        else if (collision.gameObject.name == "Magenta(Clone)" && Obj7isNeeded)
        {
            score++;
            scoreP1.text = score.ToString();
            Destroy(collision.gameObject);
            Obj7isNeeded = false;
            rand = 1;
        }
        if (score == 3)
        {
            P1Win.SetActive(true);
        }
    }
}
