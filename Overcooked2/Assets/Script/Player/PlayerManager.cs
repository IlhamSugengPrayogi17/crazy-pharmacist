﻿using Project.Networking;
using Project.Utility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Project.Player
{
    public class PlayerManager : MonoBehaviour
    {
        private GameObject table2;
        private GameObject table1;
        private GameObject table3;
        private GameObject table4;
        private Transform posTable2;
        private Transform posTable1;
        private Transform posTable3;
        private Transform posTable4;

        private GameObject table5;
        private GameObject table6;
        private GameObject table7;
        private GameObject table8;
        private Transform posTable5;
        private Transform posTable6;
        private Transform posTable7;
        private Transform posTable8;

        const float BARREL_PIVOT_OFFSET = 90.0f;

        [Header("Data")]
        [SerializeField]
        private float speed = 4;
        [SerializeField]
        private float rotation = 60;

        [Header("Object References")]
        [SerializeField]
        private Transform barrelPivot;
        [SerializeField]
        private Transform bulletSpawnPoint;
        private ValueManager valueManagerRight;
        private ValueManager valueManagerLeft;

        [Header("Data")]
        [SerializeField]
        private NetworkIdentity networkIdentity;

        private float lastRotation;

        //Shooting
        private BulletData bulletData;
        private Cooldown shootingCooldown;
        // Start is called before the first frame update
        void Start()
        {
            valueManagerLeft = GameObject.Find("Hand").GetComponent<ValueManager>();
            valueManagerRight = GameObject.Find("Hand2").GetComponent<ValueManager>();

            table1 = GameObject.FindGameObjectWithTag("meja1");
            posTable1 = table1.GetComponent<Transform>();
            table2 = GameObject.FindGameObjectWithTag("meja2");
            posTable2 = table2.GetComponent<Transform>();
            table3 = GameObject.FindGameObjectWithTag("meja3");
            posTable3 = table3.GetComponent<Transform>();
            table4 = GameObject.FindGameObjectWithTag("meja4");
            posTable4 = table4.GetComponent<Transform>();

            table5 = GameObject.FindGameObjectWithTag("meja5");
            posTable5 = table5.GetComponent<Transform>();
            table6 = GameObject.FindGameObjectWithTag("meja6");
            posTable6 = table6.GetComponent<Transform>();
            table7 = GameObject.FindGameObjectWithTag("meja7");
            posTable7 = table7.GetComponent<Transform>();
            table8 = GameObject.FindGameObjectWithTag("meja8");
            posTable8 = table8.GetComponent<Transform>();

            shootingCooldown = new Cooldown(2);
            bulletData = new BulletData();
            bulletData.position = new Position();
            bulletData.direction = new Position();
        }

        // Update is called once per frame
        public void Update()
        {
            if (networkIdentity.IsControlling())
            {
                checkMovement();
                checkRotation();
                checkShooting();
                valueManagerLeft.GetNetID(networkIdentity);
                valueManagerRight.GetNetID(networkIdentity);
                
            }
        }

        public float GetLastRotation()
        {
            return lastRotation;
        }
        public void SetRotation(float value)
        {
            barrelPivot.rotation = Quaternion.Euler(0, 0, value + BARREL_PIVOT_OFFSET);
        }

        public void checkMovement()
        {
            if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey("w"))
            {

                transform.position += transform.TransformDirection(Vector3.forward) * Time.deltaTime * speed * 2.5f;
            }
            else if (Input.GetKey("w") && !Input.GetKey(KeyCode.LeftShift))
            {
                transform.rotation = Quaternion.Euler(transform.rotation.x, 0, transform.rotation.z);
                moveForward();
            }
            else if (Input.GetKey("s"))
            {
                transform.rotation = Quaternion.Euler(transform.rotation.x, 180, transform.rotation.z);
                moveForward();
            }

            if (Input.GetKey("a") && !Input.GetKey("d"))
            {
                transform.rotation = Quaternion.Euler(transform.rotation.x, 270, transform.rotation.z);
                moveForward();
            }
            else if (Input.GetKey("d") && !Input.GetKey("a"))
            {
                transform.rotation = Quaternion.Euler(transform.rotation.x, 90, transform.rotation.z);
                moveForward();

            }
        }
        void moveForward()
        {
            transform.position += transform.TransformDirection(Vector3.forward) * speed * Time.deltaTime;
            //transform.position += new Vector3(0,0,1) * speed * Time.deltaTime;
        }
        private void checkRotation()
        {
            Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3 dif = mousePosition - transform.position;
            dif.Normalize();
            float rot = Mathf.Atan2(dif.y, dif.x) * Mathf.Rad2Deg;

            lastRotation = rot;

            //barrelPivot.rotation = Quaternion.Euler(0, 0, rot + BARREL_PIVOT_OFFSET);
        }
        private void checkShooting()
        {
            shootingCooldown.CooldownUpdate();

            if (Input.GetKeyDown(KeyCode.Alpha4) && this.gameObject.transform.position.z >= posTable4.transform.position.z - 5 && this.gameObject.transform.position.x <= posTable4.transform.position.x + 4 && this.gameObject.transform.position.x >= posTable4.transform.position.x - 4 && !shootingCooldown.IsOnCooldown())
            {
                shootingCooldown.StartCooldown();
                //Define Bullet
                bulletData.position.x = bulletSpawnPoint.position.x.TwoDecimals();
                bulletData.position.y = bulletSpawnPoint.position.y.TwoDecimals();
                bulletData.position.z = bulletSpawnPoint.position.z.TwoDecimals();
                bulletData.name = "Bullet";

                //Send Bullet
                networkIdentity.GetSocket().Emit("fireBullet", new JSONObject(JsonUtility.ToJson(bulletData)));
            }
            if (Input.GetKeyDown(KeyCode.Alpha3) && this.gameObject.transform.position.x <= posTable3.transform.position.x + 4 && this.gameObject.transform.position.x >= posTable3.transform.position.x && this.gameObject.transform.position.z <= posTable3.transform.position.z + 4 && this.gameObject.transform.position.z >= posTable3.transform.position.z - 4  && !shootingCooldown.IsOnCooldown())
            {
                shootingCooldown.StartCooldown();
                //Define Bullet
                bulletData.position.x = bulletSpawnPoint.position.x.TwoDecimals();
                bulletData.position.y = bulletSpawnPoint.position.y.TwoDecimals();
                bulletData.position.z = bulletSpawnPoint.position.z.TwoDecimals();
                bulletData.name = "Obj3";

                //Send Bullet
                networkIdentity.GetSocket().Emit("fireBullet", new JSONObject(JsonUtility.ToJson(bulletData)));
            }

            if (Input.GetKeyDown(KeyCode.Alpha2) && this.gameObject.transform.position.z <= posTable2.transform.position.z + 4 && (this.gameObject.transform.position.x <= posTable2.transform.position.x + 4) && (this.gameObject.transform.position.x >= posTable2.transform.position.x - 4) && !shootingCooldown.IsOnCooldown())
            {
                shootingCooldown.StartCooldown();

                //Define Bullet
                bulletData.position.x = bulletSpawnPoint.position.x.TwoDecimals();
                bulletData.position.y = bulletSpawnPoint.position.y.TwoDecimals();
                bulletData.position.z = bulletSpawnPoint.position.z.TwoDecimals();
                bulletData.name = "Obj4";
                //bulletData.direction.x = bulletSpawnPoint.up.x;
                //bulletData.direction.y = bulletSpawnPoint.up.y;

                //Send Bullet
                networkIdentity.GetSocket().Emit("fireBullet", new JSONObject(JsonUtility.ToJson(bulletData)));
            }
            if (Input.GetKeyDown(KeyCode.Alpha1) && (this.gameObject.transform.position.x >= posTable8.transform.position.x - 3 && (this.gameObject.transform.position.z <= posTable8.transform.position.z + 4) && (this.gameObject.transform.position.z >= posTable8.transform.position.z - 4)) && !shootingCooldown.IsOnCooldown())
            {
                shootingCooldown.StartCooldown();

                //Define Bullet
                bulletData.position.x = bulletSpawnPoint.position.x.TwoDecimals();
                bulletData.position.y = bulletSpawnPoint.position.y.TwoDecimals();
                bulletData.position.z = bulletSpawnPoint.position.z.TwoDecimals();
                bulletData.name = "Obj1";
                //bulletData.direction.x = bulletSpawnPoint.up.x;
                //bulletData.direction.y = bulletSpawnPoint.up.y;

                //Send Bullet
                networkIdentity.GetSocket().Emit("fireBullet", new JSONObject(JsonUtility.ToJson(bulletData)));
            }
            if (Input.GetKeyDown(KeyCode.Alpha8) && this.gameObject.transform.position.z >= posTable5.transform.position.z - 5 && this.gameObject.transform.position.x <= posTable5.transform.position.x + 4 && this.gameObject.transform.position.x >= posTable5.transform.position.x - 4 && !shootingCooldown.IsOnCooldown())
            {
                shootingCooldown.StartCooldown();
                //Define Bullet
                bulletData.position.x = bulletSpawnPoint.position.x.TwoDecimals();
                bulletData.position.y = bulletSpawnPoint.position.y.TwoDecimals();
                bulletData.position.z = bulletSpawnPoint.position.z.TwoDecimals();
                bulletData.name = "Bullet";

                //Send Bullet
                networkIdentity.GetSocket().Emit("fireBullet", new JSONObject(JsonUtility.ToJson(bulletData)));
            }
            if (Input.GetKeyDown(KeyCode.Alpha7) && this.gameObject.transform.position.x >= posTable6.transform.position.x - 4 && this.gameObject.transform.position.x <= posTable6.transform.position.x && this.gameObject.transform.position.z <= posTable6.transform.position.z + 4 && this.gameObject.transform.position.z >= posTable6.transform.position.z - 4 && !shootingCooldown.IsOnCooldown())
            {
                shootingCooldown.StartCooldown();
                //Define Bullet
                bulletData.position.x = bulletSpawnPoint.position.x.TwoDecimals();
                bulletData.position.y = bulletSpawnPoint.position.y.TwoDecimals();
                bulletData.position.z = bulletSpawnPoint.position.z.TwoDecimals();
                bulletData.name = "Obj3";

                //Send Bullet
                networkIdentity.GetSocket().Emit("fireBullet", new JSONObject(JsonUtility.ToJson(bulletData)));
            }
            if (Input.GetKeyDown(KeyCode.Alpha6) && (this.gameObject.transform.position.x >= posTable7.transform.position.x - 3 && (this.gameObject.transform.position.z <= posTable7.transform.position.z + 4) && (this.gameObject.transform.position.z >= posTable7.transform.position.z - 4)) && !shootingCooldown.IsOnCooldown())
            {
                shootingCooldown.StartCooldown();
                //Define Bullet
                bulletData.position.x = bulletSpawnPoint.position.x.TwoDecimals();
                bulletData.position.y = bulletSpawnPoint.position.y.TwoDecimals();
                bulletData.position.z = bulletSpawnPoint.position.z.TwoDecimals();
                bulletData.name = "Obj4";

                //Send Bullet
                networkIdentity.GetSocket().Emit("fireBullet", new JSONObject(JsonUtility.ToJson(bulletData)));
            }
            if (Input.GetKeyDown(KeyCode.Alpha5) && this.gameObject.transform.position.x <= posTable8.transform.position.x + 4 && this.gameObject.transform.position.z <= posTable8.transform.position.z + 4 && this.gameObject.transform.position.z >= posTable8.transform.position.z - 4 && !shootingCooldown.IsOnCooldown())
            {
                shootingCooldown.StartCooldown();
                //Define Bullet
                bulletData.position.x = bulletSpawnPoint.position.x.TwoDecimals();
                bulletData.position.y = bulletSpawnPoint.position.y.TwoDecimals();
                bulletData.position.z = bulletSpawnPoint.position.z.TwoDecimals();
                bulletData.name = "Obj1";

                //Send Bullet
                networkIdentity.GetSocket().Emit("fireBullet", new JSONObject(JsonUtility.ToJson(bulletData)));
            }
        }

        //private void OnCollisionEnter(Collision collision)
        //{
        //    shootingCooldown.CooldownUpdate();

        //    if (Input.GetKeyDown(KeyCode.Alpha4) && collision.gameObject == table4 && !shootingCooldown.IsOnCooldown())
        //    {
        //        shootingCooldown.StartCooldown();
        //        //Define Bullet
        //        bulletData.position.x = bulletSpawnPoint.position.x.TwoDecimals();
        //        bulletData.position.y = bulletSpawnPoint.position.y.TwoDecimals();
        //        bulletData.position.z = bulletSpawnPoint.position.z.TwoDecimals();
        //        bulletData.name = "Bullet";

        //        //Send Bullet
        //        networkIdentity.GetSocket().Emit("fireBullet", new JSONObject(JsonUtility.ToJson(bulletData)));
        //    }
        //    if (Input.GetKeyDown(KeyCode.Alpha3) && collision.gameObject == table3 && !shootingCooldown.IsOnCooldown())
        //    {
        //        shootingCooldown.StartCooldown();
        //        //Define Bullet
        //        bulletData.position.x = bulletSpawnPoint.position.x.TwoDecimals();
        //        bulletData.position.y = bulletSpawnPoint.position.y.TwoDecimals();
        //        bulletData.position.z = bulletSpawnPoint.position.z.TwoDecimals();
        //        bulletData.name = "Obj3";

        //        //Send Bullet
        //        networkIdentity.GetSocket().Emit("fireBullet", new JSONObject(JsonUtility.ToJson(bulletData)));
        //    }

        //    if (Input.GetKeyDown(KeyCode.Alpha2) && collision.gameObject == table2 && !shootingCooldown.IsOnCooldown())
        //    {
        //        shootingCooldown.StartCooldown();

        //        //Define Bullet
        //        bulletData.position.x = bulletSpawnPoint.position.x.TwoDecimals();
        //        bulletData.position.y = bulletSpawnPoint.position.y.TwoDecimals();
        //        bulletData.position.z = bulletSpawnPoint.position.z.TwoDecimals();
        //        bulletData.name = "Obj4";
        //        //bulletData.direction.x = bulletSpawnPoint.up.x;
        //        //bulletData.direction.y = bulletSpawnPoint.up.y;

        //        //Send Bullet
        //        networkIdentity.GetSocket().Emit("fireBullet", new JSONObject(JsonUtility.ToJson(bulletData)));
        //    }
        //    if (Input.GetKeyDown(KeyCode.Alpha1) && collision.gameObject == table1 && !shootingCooldown.IsOnCooldown())
        //    {
        //        shootingCooldown.StartCooldown();

        //        //Define Bullet
        //        bulletData.position.x = bulletSpawnPoint.position.x.TwoDecimals();
        //        bulletData.position.y = bulletSpawnPoint.position.y.TwoDecimals();
        //        bulletData.position.z = bulletSpawnPoint.position.z.TwoDecimals();
        //        bulletData.name = "Obj1";
        //        //bulletData.direction.x = bulletSpawnPoint.up.x;
        //        //bulletData.direction.y = bulletSpawnPoint.up.y;

        //        //Send Bullet
        //        networkIdentity.GetSocket().Emit("fireBullet", new JSONObject(JsonUtility.ToJson(bulletData)));
        //    }
        //    if (Input.GetKeyDown(KeyCode.Alpha8) && collision.gameObject == table5 && !shootingCooldown.IsOnCooldown())
        //    {
        //        shootingCooldown.StartCooldown();
        //        //Define Bullet
        //        bulletData.position.x = bulletSpawnPoint.position.x.TwoDecimals();
        //        bulletData.position.y = bulletSpawnPoint.position.y.TwoDecimals();
        //        bulletData.position.z = bulletSpawnPoint.position.z.TwoDecimals();
        //        bulletData.name = "Bullet";

        //        //Send Bullet
        //        networkIdentity.GetSocket().Emit("fireBullet", new JSONObject(JsonUtility.ToJson(bulletData)));
        //    }
        //    if (Input.GetKeyDown(KeyCode.Alpha5) && collision.gameObject == table6 && !shootingCooldown.IsOnCooldown())
        //    {
        //        shootingCooldown.StartCooldown();
        //        //Define Bullet
        //        bulletData.position.x = bulletSpawnPoint.position.x.TwoDecimals();
        //        bulletData.position.y = bulletSpawnPoint.position.y.TwoDecimals();
        //        bulletData.position.z = bulletSpawnPoint.position.z.TwoDecimals();
        //        bulletData.name = "Obj3";

        //        //Send Bullet
        //        networkIdentity.GetSocket().Emit("fireBullet", new JSONObject(JsonUtility.ToJson(bulletData)));
        //    }
        //    if (Input.GetKeyDown(KeyCode.Alpha6) && collision.gameObject == table7 && !shootingCooldown.IsOnCooldown())
        //    {
        //        shootingCooldown.StartCooldown();
        //        //Define Bullet
        //        bulletData.position.x = bulletSpawnPoint.position.x.TwoDecimals();
        //        bulletData.position.y = bulletSpawnPoint.position.y.TwoDecimals();
        //        bulletData.position.z = bulletSpawnPoint.position.z.TwoDecimals();
        //        bulletData.name = "Obj4";

        //        //Send Bullet
        //        networkIdentity.GetSocket().Emit("fireBullet", new JSONObject(JsonUtility.ToJson(bulletData)));
        //    }
        //    if (Input.GetKeyDown(KeyCode.Alpha7) && collision.gameObject == table8 && !shootingCooldown.IsOnCooldown())
        //    {
        //        shootingCooldown.StartCooldown();
        //        //Define Bullet
        //        bulletData.position.x = bulletSpawnPoint.position.x.TwoDecimals();
        //        bulletData.position.y = bulletSpawnPoint.position.y.TwoDecimals();
        //        bulletData.position.z = bulletSpawnPoint.position.z.TwoDecimals();
        //        bulletData.name = "Obj1";

        //        //Send Bullet
        //        networkIdentity.GetSocket().Emit("fireBullet", new JSONObject(JsonUtility.ToJson(bulletData)));
        //    }
        //}
    }
}