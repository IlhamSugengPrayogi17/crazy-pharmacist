﻿using Project.Networking;
using Project.Utility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValueManager : MonoBehaviour
{

    [SerializeField] private GameObject orangeRamuan, greenRamuan, purpleRamuan;
    public static int value = 0;
    [SerializeField] private NetworkIdentity networkIdentity;
    private BulletData bulletData;

    // Use this for initialization
    void Start()
    {
        bulletData = new BulletData();
        bulletData.position = new Position();
        bulletData.direction = new Position();
    }
    public void GetNetID(NetworkIdentity nID)
    {
        networkIdentity = nID;
    }
    public void SpawnObj5()
    {
        bulletData.position.x = transform.position.x.TwoDecimals();
        bulletData.position.y = transform.position.y.TwoDecimals()-5;
        bulletData.position.z = transform.position.z.TwoDecimals();
        bulletData.name = "Obj5";

        //Send Obj5
        networkIdentity.GetSocket().Emit("fireBullet", new JSONObject(JsonUtility.ToJson(bulletData)));
        value = 0;
    }

    public void SpawnObj6()
    {
        bulletData.position.x = transform.position.x.TwoDecimals();
        bulletData.position.y = transform.position.y.TwoDecimals() - 5;
        bulletData.position.z = transform.position.z.TwoDecimals();
        bulletData.name = "Obj6";

        //Send Obj5
        networkIdentity.GetSocket().Emit("fireBullet", new JSONObject(JsonUtility.ToJson(bulletData)));
        value = 0;
    }

    public void SpawnObj7()
    {
        bulletData.position.x = transform.position.x.TwoDecimals();
        bulletData.position.y = transform.position.y.TwoDecimals() - 5;
        bulletData.position.z = transform.position.z.TwoDecimals();
        bulletData.name = "Obj7";

        //Send Obj5
        networkIdentity.GetSocket().Emit("fireBullet", new JSONObject(JsonUtility.ToJson(bulletData)));
        value = 0;
    }
}
