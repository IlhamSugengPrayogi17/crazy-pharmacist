﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grabber : MonoBehaviour
{
	public GameObject grabPoint;
	private Transform cubePos;
	public float range = 0.1f;
	bool carrying = false;

	// Start is called before the first frame update
	void Start()
	{
        grabPoint = GameObject.Find("GrabPoint");
		cubePos = grabPoint.GetComponent<Transform>();
	}

	// Update is called once per frame
	void Update()
	{
		if (Input.GetKey("k") && carrying == true)/*&& (cubePos.transform.position - this.transform.position).sqrMagnitude < range)*/
		{
			gameObject.transform.position = grabPoint.transform.position;
            //grabPoint.GetComponent<GrabPointScript>().grabbing = true;
			//Debug.Log ("terpegang");
		}
        else
        {
            carrying = false;
            //grabPoint.GetComponent<GrabPointScript>().grabbing = false;
        }
    }

	void OnTriggerStay(Collider other)
	{
		if (other.gameObject == grabPoint)
		{
			carrying = true;
            //Debug.Log(carrying);
        }
    }
}
