﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obj : MonoBehaviour {

	#region Variables
	// death support
	const float EnemyLifespanSeconds = 2;
	TimerObjSpawn deathTimer;

	#endregion

	/// <summary>
	/// Use this for initialization
	/// </summary>
	void Start()
	{ 
		// create and start timer
		deathTimer = gameObject.AddComponent<TimerObjSpawn>();
		deathTimer.Duration = EnemyLifespanSeconds;
		deathTimer.Run(); 
	}

	/// <summary>
	/// Update is called once per frame
	/// </summary>
	void Update()
	{
		// destroy enemy if death timer finished
		if(deathTimer){
			if (deathTimer.Finished)
			{    
				Destroy(gameObject);       
			}
		}

	}
}
