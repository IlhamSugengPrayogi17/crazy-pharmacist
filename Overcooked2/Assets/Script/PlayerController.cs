﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PlayerController : MonoBehaviour {


	public float movementSpeed;


	//bool facingRight = false;

	//bool facingForward = true;


	void Start(){


	}



	void Update(){


		if(Input.GetKey(KeyCode.LeftShift) && Input.GetKey("w")){

			transform.position += transform.TransformDirection(Vector3.forward) * Time.deltaTime * movementSpeed * 2.5f;
		} else if(Input.GetKey("w") && !Input.GetKey(KeyCode.LeftShift)){
			transform.rotation = Quaternion.Euler(transform.rotation.x, 0, transform.rotation.z);
			moveForward();
		} else if(Input.GetKey("s")){
			transform.rotation = Quaternion.Euler(transform.rotation.x, 180, transform.rotation.z);
			moveForward();
		}

		if(Input.GetKey("a") && !Input.GetKey("d")){
			transform.rotation = Quaternion.Euler(transform.rotation.x, 270, transform.rotation.z);
			moveForward();
		} else if(Input.GetKey("d") && !Input.GetKey("a")){
			transform.rotation = Quaternion.Euler(transform.rotation.x, 90, transform.rotation.z);
			moveForward();

		}
	}
	void moveForward()
	{
		transform.position += transform.TransformDirection(Vector3.forward) * Time.deltaTime * movementSpeed;
	}
}
